## Pub/sub system using HTTP requests

----------------
Installation
---------

It is necessary to have installed:
* A version of PHP **7.2.5** or **higher**
* PHP Composer package manager
* Mysql https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/

#### Install Composer #####
* Step 1:
Go to the following web page https://getcomposer.org/
* Step 2:
Click to **Download**
* Step 3:
Click to **Composer-Setup.exe** this will open a window in your browser to save the composer file
* Step 4:
Run the downloaded file **Composer-Setup.exe** and follow the installation steps

## Prepare our Development environment

##### Run the command in console: #####

```
composer install
```

##### Generate application key #####

First you need to edit the **.env.example** to **.env** Then you need to run the command in console:

```
php artisan key:generate
```

##### Config the Database #####

Before run the migration, config the variables DB_* in **.env** to have a connection with some DB. Then you need to run the command in console:

```
php artisan migrate
```

##### Local Development Server #####

If you have PHP installed locally and you would like to use PHP's built-in development server to serve your application, you may use the serve Artisan command. This command will start a development server at http://localhost:8000:

```
php artisan serve
```

More robust local development options are available via [Homestead](https://laravel.com/docs/7.x/homestead) and [Valet](https://laravel.com/docs/7.x/valet).

## Run PHPUnit tests in console

Before executing the tests it is necessary:
* You must have a development server initialized (See the **Local Development Server**)
* You need to be at the root of the project in the console

The following test will check:
* A user cannot create a subscription without sending the url.
* A user can create a susbscription sending the url.
* Avoid duplicate subscriptions.

```
vendor/bin/phpunit tests/Feature/SubscriptionTest.php
```

The following test will check:
* Publish an event to a topic does not exist
* Publish an event to a topic

```
vendor/bin/phpunit tests/Feature/SubscriptionTest.php
```

## Run Bash Shell Script

Before executing the tests it is necessary:
* You must have a development server initialized (See the Local Development Server)
* You need to be at the root of the project in the console

You will need to make the file executable
```
chmod +x start-server.sh
```

Then you can run the bash
```
./start-server.sh
```

#### Note #### 

If you want to verify if the **http://localhost:8000/event** is receiving the information you can check the data in the **laravel.log** file. The file location is

```
storage/logs/laravel.log
```

##### WARNING #####

If you are development server with **php artisan serve** you will have a trouble with the execution of the **start-server.sh**. For more info check this [link](https://stackoverflow.com/questions/44879574/laravel-server-hangs-whenever-i-try-to-request-localhost8000-any-using-guzzle)

The following steps are a recommendation to execute the **start-server.sh**

* Step 1: Modify the curl url's in the **start-server.sh**
```
http://localhost:8000/subscribe/topic1 to http://localhost:8001/subscribe/topic1
```

```
http://localhost:8000/publish/topic1 to http://localhost:8001/publish/topic1
```

* Step 2: Open a console an run
```
php artisan serve --port 8000
```

* Step 3: Open a second console an run
```
php artisan serve --port 8001
```

* Step 4: Now you can run
```
./start-server.sh
```