<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PublishTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function publish_event_to_a_topic_does_not_exist()
    {
        // $this->withoutExceptionHandling();

        $attributes = [
            'message' => "hello",
        ];

        $response = $this->postJson('/publish/topic2', $attributes);

        $response->assertNotFound();
    }

    /** @test */
    public function publish_event_to_a_topic()
    {
        // $this->withoutExceptionHandling();

        $attributes = [
            'url' => "http://localhost:8000/event",
        ];

        $response = $this->postJson('/subscribe/topic1', $attributes);  // TODO Make a factory

        $attributes = [
            'message' => "hello",
        ];

        $response = $this->postJson('/publish/topic1', $attributes);

        $response->assertOk();
    }
}
