<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_cannot_create_a_subscription_without_webhook()
    {
        // $this->withoutExceptionHandling();

        $response = $this->postJson('/subscribe/topic1', []);

        $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /** @test */
    public function a_user_can_create_a_subscription()
    {
        // $this->withoutExceptionHandling();

        $attributes = [
            'url' => "http://localhost:8000/event",
        ];

        $response = $this->postJson('/subscribe/topic1', $attributes);

        $response->assertOk();
    }

    /** @test */
    public function avoid_duplicate_subscriptions()
    {
        // $this->withoutExceptionHandling();

        $attributes = [
            'url' => "http://localhost:8000/event",
        ];

        $response = $this->postJson('/subscribe/topic1', $attributes);

        $response->assertOk();

        $attributes = [
            'url' => "http://localhost:8000/EvenT",
        ];

        $response = $this->postJson('/subscribe/topic1', $attributes);

        $response->assertOk();
    }
}
