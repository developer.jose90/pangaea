<?php

namespace App\Jobs;

use App\Models\TopicEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class ProcessTopicEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $topicEvent;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TopicEvent $topicEvent)
    {
        $this->topicEvent = $topicEvent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptions = $this->topicEvent->topic->subscriptions;

        foreach ($subscriptions as $subscription) {
            \Log::info("Send data to webhook subscription {$subscription->webhook}");

            $response = Http::post($subscription->webhook, [
                'topic' => $this->topicEvent->topic->name,
                'content' => json_decode($this->topicEvent->content, true),
            ]);

            $response->throw();            
        }

        $this->topicEvent->delete();
    }
}
