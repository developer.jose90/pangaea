<?php

namespace App\Providers;

use App\Repositories\Subscription\SubscriptionInterface;
use App\Repositories\Subscription\SubscriptionRepository;
use Illuminate\Support\ServiceProvider;

class SubscriptionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SubscriptionInterface::class,
            SubscriptionRepository::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            SubscriptionInterface::class,
        ];
    }
}
