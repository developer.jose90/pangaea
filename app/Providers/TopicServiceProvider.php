<?php

namespace App\Providers;

use App\Repositories\Topic\TopicInterface;
use App\Repositories\Topic\TopicRepository;
use App\Repositories\TopicEvent\TopicEventInterface;
use App\Repositories\TopicEvent\TopicEventRepository;
use Illuminate\Support\ServiceProvider;

class TopicServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TopicInterface::class,
            TopicRepository::class
        );

        $this->app->bind(
            TopicEventInterface::class,
            TopicEventRepository::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            TopicInterface::class,
            TopicEventInterface::class,
        ];
    }
}
