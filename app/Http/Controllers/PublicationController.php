<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Services\TopicEventService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PublicationController extends Controller
{
    protected $topicEventService;

    public function __construct(TopicEventService $topicEventService)
    {
        $this->topicEventService = $topicEventService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Topic $topic)
    {
        $this->topicEventService->create($topic, $request->all());

        return $this->jsonResponseMessage('Publish event created');
    }
}
