<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        \Log::info("Data of topic:");
        \Log::info($request->all());

        return response(['message' => "Success"])->setStatusCode(Response::HTTP_OK);
    }
}
