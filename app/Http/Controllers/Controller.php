<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return a JSON response with 'message' key.
     *
     * @param  string  $message
     * @param  int    $status
     *
     * @return \Illuminate\Http\Response
     */
    public function jsonResponseMessage(string $message, int $status = 200): Response
    {
        return response(['message' => $message])->setStatusCode($status);
    }
}
