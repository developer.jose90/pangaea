<?php

namespace App\Http\Rules;

class TopicEventRules extends BaseRules
{
    public function rules($requestMethod = null): array
    {
        return $this->getValidationRules($requestMethod);
    }

    protected function validatePost(): array
    {
        return [
            'topic_id' => 'required|exists:topics,id',
            'content' => 'required|json|string|max:65535',
        ];
    }
}
