<?php

namespace App\Http\Rules;

class SubscriptionRules extends BaseRules
{
    public function rules($requestMethod = null): array
    {
        return $this->getValidationRules($requestMethod);
    }

    protected function validatePost(): array
    {
        return [
            'webhook' => 'required|string|max:255',
        ];
    }
}
