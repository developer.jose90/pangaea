<?php

namespace App\Http\Rules;

class TopicRules extends BaseRules
{
    public function rules($requestMethod = null): array
    {
        return $this->getValidationRules($requestMethod);
    }

    protected function validatePost(): array
    {
        return [
            'name' => 'required|string|max:255',
        ];
    }
}
