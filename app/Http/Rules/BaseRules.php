<?php

namespace App\Http\Rules;

class BaseRules
{
    public function getValidationRules($requestMethod = null): array
    {
        $methodName = sprintf(
            'validate%s',
            ucfirst($requestMethod ?? request()->method())
        );

        return (method_exists($this, $methodName))
            ? $this->$methodName()
            : [];
    }
}
