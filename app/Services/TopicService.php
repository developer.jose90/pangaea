<?php

namespace App\Services;

use DB;
use App\Http\Rules\TopicRules;
use App\Repositories\Topic\TopicInterface;

class TopicService extends BaseService
{
    protected $topicRepo;


    public function __construct(TopicInterface $topicRepo)
    {
        $this->topicRepo = $topicRepo;
    }

    /**
     * Get the topic by name. If the topic does not exist then it will be created.
     *
     * @param  string  $name
     *
     * @return \App\Models\Subscription
     */
    public function getTopic($name)
    {
        DB::beginTransaction();

        $topic = $this->topicRepo->findByName($name);

        if (is_null($topic)) {
            $params = $this->validateRequest(TopicRules::class, [
                'name' => $name,
            ]);

            $topic = $this->topicRepo->create($params);
        }

        DB::commit();

        return $topic;
    }
}
