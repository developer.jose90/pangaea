<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class BaseService
{
    /**
     * Validate service request.
     *
     * @param  string  $ruleClassName
     * @param  \Illuminate\Http\Request|array  $request
     *
     * @throws \Exception
     *
     * @return void
     */
    public function validateRequest(string $ruleClassName, $request, $requestMethood = null, string $message = null)
    {
        if ($request instanceof Request) {
            $request = $request->all();
        }

        $validator = Validator::make(
            $request, (new $ruleClassName())->rules($requestMethood)
        );


        if ($validator->fails()) {
            $errorString = implode(",", $validator->messages()->all());

            throw new Exception(
                $message ?? $errorString,
                1
            );
        }

        return $validator->validated();
    }
}
