<?php

namespace App\Services;

use App\Http\Rules\TopicEventRules;
use App\Jobs\ProcessTopicEvent;
use App\Models\Topic;
use App\Repositories\TopicEvent\TopicEventInterface;

class TopicEventService extends BaseService
{
    protected $topicEventRepo;

    public function __construct(TopicEventInterface $topicEventRepo)
    {
        $this->topicEventRepo = $topicEventRepo;
    }

    /**
     * Create a topic event and queue the event for the suscribers.
     *
     * @param  string  $name
     *
     * @return \App\Models\Subscription
     */
    public function create(Topic $topic, $data)
    {
        $params = $this->validateRequest(TopicEventRules::class, [
            'topic_id' => $topic->id,
            'content' => json_encode($data),
        ]);

        $topicEvent = $this->topicEventRepo->create($params);

        ProcessTopicEvent::dispatchNow($topicEvent);
    }
}
