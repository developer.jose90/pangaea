<?php

namespace App\Services;

use DB;
use App\Http\Rules\SubscriptionRules;
use App\Http\Rules\TopicRules;
use App\Repositories\Subscription\SubscriptionInterface;

class SubscriptionService extends BaseService
{
    protected $topicService;

    protected $subscriptionRepo;

    public function __construct(SubscriptionInterface $subscriptionRepo,
        TopicService $topicService)
    {
        $this->subscriptionRepo = $subscriptionRepo;

        $this->topicService = $topicService;
    }

    /**
     * Store a subscription.
     *
     * @param  \Illuminate\Http\Request|array  $request
     *
     * @return void
     */
    public function create($request, $topicName)
    {
        DB::beginTransaction();

        $topic = $this->topicService->getTopic($topicName);

        $params = $this->validateRequest(SubscriptionRules::class, [
            'webhook' => $this->getWebhook($request),
        ]);

        $result = $this->subscriptionRepo->create($topic, $params);

        DB::commit();
    }

    private function getWebhook($request)
    {
        if ($request->headers->get('Content-Type') == "application/x-www-form-urlencoded") {
            $body = json_decode($request->getContent(), true);

            return $body['url'];
        }

        if ($request->headers->get('Content-Type') == "application/json") {
            return $request->url;
        }
    }
}
