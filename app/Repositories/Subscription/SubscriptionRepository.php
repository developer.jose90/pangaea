<?php

namespace App\Repositories\Subscription;

use Illuminate\Contracts\Container\Container;
use App\Models\Subscription;
use App\Models\Topic;

class SubscriptionRepository implements SubscriptionInterface
{
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Contracts\Container\Container $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->model = $app[Subscription::class];
    }

    public function findByWebhook($webhook)
    {
        return $this->model
            ->whereWebhook($webhook)
            ->first();
    }

    public function create(Topic $topic, array $input)
    {
        if ($subscription = $this->findByWebhook($input['webhook'])) {
            return $subscription;
        }

        return $topic->subscriptions()->create($input);
    }
}
