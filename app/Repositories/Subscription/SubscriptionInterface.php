<?php

namespace App\Repositories\Subscription;

use App\Models\Topic;

interface SubscriptionInterface
{
	public function findByWebhook($webhook);

    public function create(Topic $topic, array $data);
}
