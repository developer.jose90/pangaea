<?php

namespace App\Repositories\Topic;

use App\Models\Topic;
use Illuminate\Contracts\Container\Container;

class TopicRepository implements TopicInterface
{
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Contracts\Container\Container $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->model = $app[Topic::class];
    }

    public function findByName(string $name)
    {
        return $this->model
            ->whereName($name)
            ->first();
    }

    public function create(array $input)
    {
        return $this->model::create($input);
    }
}
