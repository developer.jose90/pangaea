<?php

namespace App\Repositories\Topic;

interface TopicInterface
{
    public function findByName(string $name);

    public function create(array $data);
}
