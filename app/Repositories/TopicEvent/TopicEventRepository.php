<?php

namespace App\Repositories\TopicEvent;

use App\Models\TopicEvent;
use Illuminate\Contracts\Container\Container;

class TopicEventRepository implements TopicEventInterface
{
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Contracts\Container\Container $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->model = $app[TopicEvent::class];
    }

    public function create(array $input)
    {
        return $this->model::create($input);
    }
}
