<?php

namespace App\Repositories\TopicEvent;

interface TopicEventInterface
{
    public function create(array $data);
}
