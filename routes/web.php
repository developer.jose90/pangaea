<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/subscribe/{topic}', ['as' => 'store', 'uses' => 'SubscriptionController@store']);

Route::post('/publish/{topic:name}', ['as' => 'store', 'uses' => 'PublicationController@store']);

Route::post('/event', ['as' => 'store', 'uses' => 'EventController@index']);